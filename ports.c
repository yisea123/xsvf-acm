/*
 * file: ports.c
 * This file is part of the xsvf-acm utility
 *
 * Copyright 2011 Benjamin Leperchey
 * Copyright 2014-2016 Tormod Volden
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>

#include "ports.h"

// void Delay( uint32_t us );

#define XSVF_GPIO GPIOA
#define XSVF_RCC_GPIO RCC_GPIOA
#define XSVF_GPIO_BSRR GPIOA_BSRR
#define XSVF_GPIO_IDR GPIOA_IDR
#define TCK_Pin GPIO0	/* PA0 = Maple pin 11 */
#define TDO_Pin GPIO1	/* PA1 = Maple pin 10 */
#define TDI_Pin GPIO2	/* PA2 = Maple pin 9  */
#define TMS_Pin GPIO3	/* PA3 = Maple pin 8  */

void ports_init(void)
{
        // Enable Peripheral Clocks
	rcc_periph_clock_enable(XSVF_RCC_GPIO);
        // Configure output pins
	gpio_set_mode(XSVF_GPIO, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_PUSHPULL, TMS_Pin | TDI_Pin | TCK_Pin);
        // Configure input pin
	gpio_set_mode(XSVF_GPIO, GPIO_MODE_INPUT,
	              GPIO_CNF_INPUT_FLOAT, TDO_Pin);
}

void set_port(uint8_t p,int val)
{
	static uint32_t BSRR_TMS, BSRR_TDI;

	if (p == TMS && val == 0) BSRR_TMS = (TMS_Pin << 16);
	if (p == TMS && val == 1) BSRR_TMS = (TMS_Pin);
	if (p == TDI && val == 0) BSRR_TDI = (TDI_Pin << 16);
	if (p == TDI && val == 1) BSRR_TDI = (TDI_Pin);

	/* clock TMS and TDI on falling TCK */
	if (p == TCK) {
		if (val == 0) {
			XSVF_GPIO_BSRR = BSRR_TMS | BSRR_TDI | (TCK_Pin << 16);
		} else {
			XSVF_GPIO_BSRR = TCK_Pin;
		}
	}
}

void pulse_clock()
{
	set_port(TCK,0);
	set_port(TCK,1);
	set_port(TCK,0);
}

int read_tdo()
{
	return (XSVF_GPIO_IDR & TDO_Pin) ? 1 : 0 ;
}

/* Wait at least the specified number of microsec. */
void delay(long microsec)
{
//	_delay_ms(microsec>>12);
	set_port(TCK,0);
	while (--microsec > 0) {
		set_port(TCK,1);
		set_port(TCK,0);
	}
}
